ARG MAMBA_VERSION=latest
FROM registry.gitlab.com/ifb-elixirfr/docker-images/mamba:${MAMBA_VERSION}

ARG TOOL_NAME=python
ARG TOOL_VERSION=3.7

RUN ${CONDA_BIN} init

RUN wget -q https://gitlab.com/ifb-elixirfr/cluster/tools/-/raw/master/tools/${TOOL_NAME}/${TOOL_VERSION}/env.yml

RUN ${MAMBA_BIN} env create -f env.yml