## Docker Image

Should be used to build a docker image with any conda tools from https://gitlab.com/ifb-elixirfr/cluster/tools/-/tree/master/tools

- Example 1

In your shell:
```
docker build -t tools-python-2.7 --build-arg TOOL_NAME=python --build-arg TOOL_VERSION=2.7 .
docker run -it tools-python-2.7 /bin/bash
```

In the container:
```
python --version
conda activate python-2.7
python --version
```

- Example 2

In your shell:
```
docker run -it registry.gitlab.com/ifb-elixirfr/docker-images/tools:r-3.6.3 conda run -n r-3.6.3 R --version
```

- Purpose

Be able to execute some action in a virtual ifb tools env. For example install R package.

(It may be used also to run any conda ifb tool not dockerised yet ;) )

- To add an env, edit [.gitlab-ci.yml](.gitlab-ci.yml) and add a build step for your env
